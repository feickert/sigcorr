import unittest

import jax
import numpy as np
from numpy import random

from sigcorr.models.gross_vitells import GrossVitells


REL_DELTA = 1E-7


class TestGrossVitells(unittest.TestCase):
    def setUp(self):
        np.random.seed(145)

    def test_gross_vitells_b_grad(self):
        for i in range(10):
            self._one_gross_vitells_b_grad()

    def _one_gross_vitells_b_grad(self):
        xs = np.arange(20, 50, 0.5)
        model = GrossVitells(xs).init()
        p = np.array([model.B0+0.1], dtype=np.float64)

        sample = model.get_bg_samples(1)[0]

        test_b_loglike_grad = model.b_minus_loglike_grad(p, sample)
        true_b_loglike_grad = jax.grad(model.b_minus_loglike)(p, sample)
        self.assertAlmostEqual(np.max(np.absolute((test_b_loglike_grad - true_b_loglike_grad)/true_b_loglike_grad)),
                               0, delta=REL_DELTA)

    def test_gross_vitells_sb_grad(self):
        for i in range(10):
            self._one_gross_vitells_sb_grad()

    def _one_gross_vitells_sb_grad(self):
        xs = np.arange(20, 50, 0.5)
        model = GrossVitells(xs).init()
        p = np.array([300, model.B0], dtype=np.float64)
        signal_x = xs[25]

        sample = model.get_bg_samples(1)[0]

        test_sb_loglike_grad = model.sb_minus_loglike_grad(p, sample, signal_x)
        true_sb_loglike_grad = jax.grad(model.sb_minus_loglike)(p, sample, signal_x)
        self.assertAlmostEqual(np.max(np.absolute((test_sb_loglike_grad - true_sb_loglike_grad)/true_sb_loglike_grad)),
                               0, delta=REL_DELTA)

    def test_gross_vitells_b_hess(self):
        for i in range(10):
            self._one_gross_vitells_b_hess()

    def _one_gross_vitells_b_hess(self):
        xs = np.arange(20, 50, 0.5)
        model = GrossVitells(xs).init()
        p = np.array([model.B0], dtype=np.float64)

        sample = model.get_bg_samples(1)[0]

        test_b_loglike_hess = model.b_minus_loglike_hess(p, sample)
        true_b_loglike_hess = jax.hessian(model.b_minus_loglike)(p, sample)
        self.assertAlmostEqual(np.max(np.absolute((test_b_loglike_hess - true_b_loglike_hess)/true_b_loglike_hess)),
                               0, delta=REL_DELTA)

    def test_gross_vitells_sb_hess(self):
        for i in range(10):
            self._one_gross_vitells_sb_hess()

    def _one_gross_vitells_sb_hess(self):
        xs = np.arange(20, 50, 0.5)
        model = GrossVitells(xs).init()
        p = np.array([300, model.B0], dtype=np.float64)
        signal_x = xs[25]

        sample = model.get_bg_samples(1)[0]

        test_sb_loglike_hess = model.sb_minus_loglike_hess(p, sample, signal_x)
        true_sb_loglike_hess = jax.hessian(model.sb_minus_loglike)(p, sample, signal_x)
        self.assertAlmostEqual(np.max(np.absolute((test_sb_loglike_hess - true_sb_loglike_hess)/true_sb_loglike_hess)),
                               0, delta=REL_DELTA)
