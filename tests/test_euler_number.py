import numpy as np
from sigcorr.tools.euler_number import euler_number_2d
from sigcorr.tools.euler_number import euler_number_3d


SAMPLE = np.array(
    [[0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
     [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
     [1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0],
     [0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1],
     [0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1]]
)

SAMPLE_3D = np.zeros((6, 6, 6), dtype=np.uint8)
SAMPLE_3D[1:3, 1:3, 1:3] = 1
SAMPLE_3D[3, 2, 2] = 1


def test_euler_number_2d3d():
    en = euler_number_2d(SAMPLE)
    assert en == 0

    SAMPLE_mod = SAMPLE.copy()
    SAMPLE_mod[7, -3] = 0
    en = euler_number_2d(SAMPLE_mod)
    assert en == -1

    en = euler_number_3d(SAMPLE_3D)
    assert en == 1

    # for convex body, Euler number is 1
    SAMPLE_3D_2 = np.zeros((100, 100, 100))
    SAMPLE_3D_2[40:60, 40:60, 40:60] = 1
    en = euler_number_3d(SAMPLE_3D_2)
    assert en == 1

    SAMPLE_3D_3 = SAMPLE_3D_2.copy()
    SAMPLE_3D_3[45:55, 45:55, 45:55] = 0
    en = euler_number_3d(SAMPLE_3D_3)
    assert en == 2

    two_samples = np.array([SAMPLE, SAMPLE_mod])
    en = euler_number_2d(two_samples)
    assert not np.any(en - np.array([0, -1]))

    two_samples_3D = np.array([SAMPLE_3D_2, SAMPLE_3D_3])
    en = euler_number_3d(two_samples_3D)
    assert not np.any(en - np.array([1, 2]))
