import unittest
import numpy as np
from sigcorr.tools.stats.gp.euler_number import GPEulerNumberPropagator


class TestGPEulerNumPropagator(unittest.TestCase):
    def test_reciprocal(self):
        ref_sigs = np.array([0.7, 1])
        ref_euler = np.array([5.1, 8.3])
        propagator = GPEulerNumberPropagator(ref_sigs, ref_euler)
        prediction = propagator.calc(ref_sigs)
        self.assertTrue(np.allclose(prediction, ref_euler))
