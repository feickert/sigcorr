import sys
import numpy as np

masses = np.linspace(100, 160, 181)
sigma_m = np.linspace(1, 10, 21)
grid = np.array(np.meshgrid(masses, sigma_m, indexing="ij"))
grid = np.moveaxis(grid, [0], [-1])
sys.stdout.write(f"##shape {grid.shape}\n")
np.savetxt(sys.stdout, grid.ravel(), newline=" ", fmt="%a")
sys.stdout.write("\n")
