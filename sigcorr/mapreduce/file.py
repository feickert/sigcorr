import sys

from typing import List
from datetime import datetime
from multiprocessing import Process
from multiprocessing import Queue

import h5py
from tqdm import tqdm

from sigcorr.config import CFG
from sigcorr.tools.utils import produce_batch_ranges
from sigcorr.mapreduce.map_reducers import Calculator
from sigcorr.mapreduce.map_reducers import Reducer


def h5_batch_mapreduce(input_file: str, fields: List[str], batchsize: int, mapper: Calculator, reducer: Reducer):
    """
    Read samples from ``input_file`` and apply ``mapper`` to batches of size ``batchsize``,
    then aggregate processed batches with ``reducer``.

    Parameters
    ----------
        input_file : str
            path to the input HDF5 file.
        fields : List[str]
            which fields to extract and pass to the mapper.
        batchsize : int
            size of batches split along the first dimension of each extracted ``field``.
        mapper : Calculator
            instance of the :py:class:`~sigcorr.mapreduce.map_reducers.Calculator` that processes batches
        reducer : Reducer
            instance of the :py:class:`~sigcorr.mapreduce.map_reducers.Reducer` that aggregates batches

    """
    in_q = Queue()
    out_q = Queue()
    ref_seed = int(datetime.now().timestamp())//2
    processes = [Process(target=_h5_process_batches,
                         args=(in_q, input_file, fields, mapper, out_q))
                 for i in range(CFG.FITTER.bfit_pool_size)]
    [p.start() for p in processes]
    num_batches = 0
    with h5py.File(input_file, "r", swmr=True) as fin:
        n_samples = fin[fields[0]].shape[0]
    for brange in produce_batch_ranges(n_samples, batchsize):
        num_batches += 1
        in_q.put(brange)
    [in_q.put(None) for _ in processes]
    results_iterator = _h5_batch_iterresults(out_q, batchsize, num_batches)
    try:
        yield from reducer.reduce_batches(results_iterator, n_samples)
        [p.join() for p in processes]
    finally:
        [p.kill() for p in processes]


def _h5_process_batches(q_in, input_file, fields, mapper, q_out):
    with h5py.File(input_file, "r", swmr=True) as fin:
        while True:
            brange = q_in.get()
            if brange is None:
                return
            samples = [fin[field][brange, ...] for field in fields]
            res = mapper.process_batch(*samples)
            q_out.put(res)


def _h5_batch_iterresults(batch_res_q, approx_batchsize, num_batches):
    for _ in tqdm(range(num_batches),
                  total=num_batches, unit_scale=approx_batchsize, file=sys.stderr):
        one_batch_res = batch_res_q.get()
        yield one_batch_res
