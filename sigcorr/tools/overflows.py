import numpy as np


def overflows(curves, thresholds):
    return (curves[..., np.newaxis] > thresholds).any(axis=tuple(range(1, curves.ndim)))
