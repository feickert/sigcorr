import numpy as np
import skimage as ski

from sigcorr.tools.upcross import zero_upcross_count


def euler_number_along_zero_ax(sigs: np.array):
    """Compute Euler number of an array of images.

    1D case counts upcrossings,
    2D and 3D cases use skimage and loop over zero-axis with python loop

    Parameters
    ----------
    sigs : np.array
        Array of images, where the first (zero) axis enumerates images
    """
    image_dims = sigs.ndim - 1
    if image_dims > 3:
        raise NotImplementedError("Dimensions higher than 3 are not supported")
    if image_dims == 3:
        return euler_number_3d(sigs)
    if image_dims == 2:
        return euler_number_2d(sigs)
    if image_dims == 1:
        return euler_number_1d(sigs)
    raise NotImplementedError("0-dim euler num is not supported")


def euler_number_1d(sigs):
    return (sigs[..., 0] > 0) + zero_upcross_count(sigs)


def euler_number_2d(sigs):
    # connectivity chosen in consistency with MATLAB
    res_shape = sigs.shape[:-2]
    if not res_shape:
        return ski.measure.euler_number(sigs, connectivity=2)
    res = np.zeros(res_shape)
    for idx in np.ndindex(res_shape):
        res[idx] = ski.measure.euler_number(sigs[idx], connectivity=2)
    return res


def euler_number_3d(sigs):
    # connectivity left default to skimage
    res_shape = sigs.shape[:-3]
    if not res_shape:
        return ski.measure.euler_number(sigs)
    res = np.zeros(res_shape)
    for idx in np.ndindex(res_shape):
        res[idx] = ski.measure.euler_number(sigs[idx])
    return res
