from typing import List
from typing import Optional

import numpy as np
from scipy import ndimage as ndi
import scipy as sp
from scipy import interpolate  # noqa; pylint: disable=unused-import


def derivative_along_axes(arr: np.array,
                          xs_axes: List[np.array],
                          der_axes: Optional[List[int]] = None):
    """Compute derivative of the multidimensional along a subset of axes.

    Parameters
    ----------
    arr : np.array
        arr
    xs_axes : List[np.array]
        List of coordinates, 1D array per axis
    der_axes : Optional[List[int]]
        List of orders of the derivatives along each axis
    """
    if der_axes is None:
        der_axes = [0 for _ in xs_axes]
    try:
        return spline3_der_along_axes(arr, xs_axes, der_axes)
    except NotImplementedError:
        return finite_diff_der_along_axes(arr, xs_axes, der_axes)


def finite_diff_der_along_axes(arr, xs_axes, der_axes):
    res = arr
    for i, xs in enumerate(xs_axes):
        for _ in range(der_axes[i]):
            res = finite_diff_der1_along_axis(res, xs, i)
    return res


def finite_diff_der1_along_axis(arr, xs, ax):
    # kernel is reversed because convolution will reverse it back
    res = ndi.convolve1d(arr, [1, 0, -1], mode="nearest", axis=ax)
    dx = ndi.convolve(xs, [1, 0, -1], mode="nearest")
    return res/dx


def spline3_der_along_axes(arr, xs_axes, der_axes):
    if len(xs_axes) != 2:
        raise NotImplementedError()
    return spline3_der_along_2axes(arr, *xs_axes, *der_axes)


def spline3_der_along_2axes(arr, xs, ys, dx, dy):
    spline_obj = sp.interpolate.RectBivariateSpline(xs, ys, arr)
    return spline_obj(xs, ys, dx=dx, dy=dy)
