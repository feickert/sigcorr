import sys

import numpy as np
import scipy as sp
from scipy import stats


def get_delta_sigs(b_loglikes, sb_loglikes, signs=None):
    ts_batch = get_delta_ts(b_loglikes, sb_loglikes)
    sigs_batch = np.sqrt(ts_batch)
    signed_sigs_batch = sigs_batch*np.sign(signs) if signs is not None else sigs_batch
    return signed_sigs_batch


def get_delta_ts(b_loglikes, sb_loglikes):
    ts_dim = sb_loglikes.ndim - b_loglikes.ndim
    extra_b_axes = [i for i in range(b_loglikes.ndim, b_loglikes.ndim + ts_dim)]
    ts_batch = -2*(np.expand_dims(b_loglikes, axis=extra_b_axes) - sb_loglikes)
    ts_batch[ts_batch < 0] = 0
    return ts_batch


def pval2sig(pval):
    return sp.stats.norm.isf(pval)


def sig2pval(sig):
    return (1-sp.stats.norm.cdf(sig))
