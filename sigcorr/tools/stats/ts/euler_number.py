import scipy as sp
from scipy import stats

from sigcorr.tools.stats.ts.upcross import propagate_upcross


def avg_ts_euler_number_propagated(local_ts, noise_dof, ref_ts, ref_euler_num):
    ref_upcross = ref_euler_num - (1-sp.stats.chi2(df=noise_dof).cdf(ref_ts))
    propagated_upcross = propagate_upcross(ref_ts, ref_upcross, noise_dof, local_ts)
    return avg_ts_euler_number(local_ts, noise_dof, propagated_upcross)


def avg_ts_euler_number(local_ts, noise_dof, upcross):
    return (1-sp.stats.chi2(df=noise_dof).cdf(local_ts)) + upcross
