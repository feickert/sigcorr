import numpy as np


def propagate_upcross(ref_ts, ref_upcross, noise_dof, target_ts):
    return ref_upcross*np.power(target_ts/ref_ts, (noise_dof-1)/2)*np.exp(-(target_ts - ref_ts)/2)
