"""
Usage example:

.. code-block:: python

    cov_calc = BatchStats2(samples.shape[0], samples.shape[1:])
    for batch in read_in_batches(samples):
        sigs = get_sigs(batch)
        cov_calc.push(sigs)
    cov, cov_err = cov_calc.get_cov_and_err()
    corr, corr_err = cov_calc.get_corr_and_err()
"""

import numpy as np


class BatchStats1:
    def __init__(self, n_tot, sample_dim):
        dim = sample_dim
        self.n = n_tot
        self.x = np.zeros(dim)
        self.xx = np.zeros(dim)

    def push(self, batch):
        self.x += batch.sum(axis=0)/self.n
        self.xx += (batch*batch).sum(axis=0)/self.n

    def get_mean(self):
        return self.x

    def get_var(self, override_x=None):
        x = self.x if override_x is None else override_x
        variance = self.xx - x**2/self.n
        return variance

    def get_stat_err(self, override_x=None):
        return np.sqrt(self.get_var(override_x=override_x)/self.n)


class BatchStats2:
    def __init__(self, n_tot, sample_dim):
        dim = sample_dim
        self.n = n_tot
        self.x = np.zeros(dim)
        self.xy = np.zeros((dim, dim))
        self.xyy = np.zeros((dim, dim))
        self.xxyy = np.zeros((dim, dim))

    def push(self, batch):
        self.x += batch.sum(axis=0)/self.n
        self.xy += np.dot(batch.T, batch)/self.n
        sqr_batch = batch**2
        self.xyy += np.dot(batch.T, sqr_batch)/self.n
        self.xxyy += np.dot(sqr_batch.T, sqr_batch)/self.n

    def get_mean(self):
        return self.x

    def get_var(self, override_x=None):
        return np.diagonal(self.get_cov(override_x=override_x))

    def get_stat_err(self, override_x=None):
        return np.sqrt(self.get_var(override_x=override_x)/self.n)

    def get_cov(self, override_x=None):
        x = self.x if override_x is None else override_x
        x = x.reshape(1, -1)
        cov = self.xy - np.dot(x.T, x)
        return cov

    def get_cov_var(self, override_x=None):
        x = self.x if override_x is None else override_x
        x = x.reshape(1, -1)
        sqr_x = x**2
        xx = np.diagonal(self.xy).reshape(1, -1)
        var = self.xxyy - self.xy**2 + np.dot(sqr_x.T, xx) + np.dot(xx.T, sqr_x) \
            - 2*x.T*self.xyy - 2*self.xyy.T*x \
            + 6*np.dot(x.T, x)*self.xy - 4*np.dot(sqr_x.T, sqr_x)
        return var

    def get_cov_stat_err(self, override_x=None):
        cov_var = self.get_cov_var(override_x=override_x)
        return np.sqrt(cov_var/self.n)

    def get_corr(self, override_x=None):
        cov = self.get_cov(override_x=override_x)
        xx = np.diagonal(cov).reshape(1, -1)
        norm = np.dot(xx.T, xx)
        return cov/np.sqrt(norm)

    def get_corr_var(self, override_x=None):
        cov_var = self.get_cov_var(override_x=override_x)
        cov = self.get_cov(override_x=override_x)
        xx = np.diagonal(cov).reshape(1, -1)
        norm = np.dot(xx.T, xx)
        return cov_var/norm

    def get_corr_stat_err(self, override_x=None):
        corr_var = self.get_corr_var(override_x=override_x)
        return np.sqrt(corr_var/self.n)
