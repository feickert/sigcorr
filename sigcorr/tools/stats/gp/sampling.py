import numpy as np
import scipy as sp
from scipy import stats


def draw_mv_gp(cov, num):
    dist = get_mv_dist(cov)
    return draw_gp_from_dist(dist, num)


def get_mv_dist(cov):
    return sp.stats.multivariate_normal(mean=None, cov=cov, allow_singular=True)


def draw_gp_svd(cov, num):
    sqrt_cov = get_svd_sqrtcov(cov)
    return draw_gp_from_sqrtcov(sqrt_cov, num)


def get_svd_sqrtcov(cov):
    u, s, _ = np.linalg.svd(cov)
    sqrt_cov = np.sqrt(s).reshape(1, -1)*u
    return sqrt_cov


def draw_gp_chol(cov, num):
    sqrt_cov = get_chol_sqrtcov(cov)
    return draw_gp_from_sqrtcov(sqrt_cov, num)


def get_chol_sqrtcov(cov):
    sqrt_cov = np.linalg.cholesky(cov)
    return sqrt_cov


def draw_gp_from_sqrtcov(sqrtcov, num):
    return np.matmul(sqrtcov, sp.stats.norm().rvs((num, sqrtcov.shape[0])).T).T


def draw_gp_from_dist(dist, num):
    return dist.rvs(num)


def get_covariance_from_kernel(kernel, xs):
    return kernel(xs[np.newaxis, :], xs[:, np.newaxis])


def gibbs_kernel(l, x1, x2):
    return np.sqrt(2*l(x1)*l(x2)/(l(x1)**2 + l(x2)**2))*np.exp(-(x1-x2)**2/(l(x1)**2 + l(x2)**2))
