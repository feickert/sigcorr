import numpy as np
from numpy import polynomial
from numpy import linalg
import scipy as sp
from scipy import stats
from sigcorr.tools.stats.utils import sig2pval


class GPEulerNumberPropagator:
    def __init__(self, ref_sigs, ref_euler_nums):
        self.Ns = get_gp_euler_number_coefs(ref_sigs, ref_euler_nums)

    def calc(self, sigs):
        return 1 - sp.stats.norm().cdf(sigs) + np.polynomial.hermite_e.hermeval(sigs, self.Ns)*np.exp(-sigs**2/2)


def get_gp_euler_number_coefs(ref_sigs, ref_euler_nums):
    A = np.polynomial.hermite_e.hermevander(ref_sigs, ref_sigs.shape[0]-1)
    b = (ref_euler_nums - 1 + sp.stats.norm().cdf(ref_sigs))*np.exp(ref_sigs**2/2)
    x = np.linalg.solve(A, b)
    return x


def avg_gp_euler_number(local_sig, avg_upcross):
    return sig2pval(local_sig) + avg_upcross
