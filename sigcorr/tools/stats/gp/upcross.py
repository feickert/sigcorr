import numpy as np
import scipy as sp
from scipy import stats
from scipy import integrate

from sigcorr.tools.derivative import derivative_along_axes


def gp_upcross_at_level(xs, K, u):
    density = gp_upcross_density(xs, K, u)
    return integrate_gp_upcross_density(density, xs)


def integrate_gp_upcross_density(density, xs):
    return sp.integrate.simpson(density, xs)


def gp_upcross_density(xs, K, u):
    return _gp_upcross_density(xs, K, u, derivative_along_axes)


def _gp_upcross_density(xs, K, u, derivative_func):
    K_xdy = derivative_func(K, [xs, xs], [0, 1])
    K_dxdy = derivative_func(K, [xs, xs], [1, 1])

    sigma_x = np.sqrt(np.diagonal(K))
    sigma_dx = np.sqrt(np.diagonal(K_dxdy))

    rho_xdy = np.diagonal(K_xdy)/sigma_x/sigma_dx

    mu_star = rho_xdy*sigma_dx/sigma_x
    sigma_star = sigma_dx*np.sqrt(1 - rho_xdy**2)

    return 1/np.sqrt(2*np.pi)/sigma_x*np.exp(-u**2/2/sigma_x**2) * \
        (mu_star*u*sp.stats.norm.cdf(mu_star/sigma_star*u) +
         sigma_star/np.sqrt(2*np.pi)*np.exp(-mu_star**2/2/sigma_star**2*u**2))


def gp_upcross_density_stationary(xs, K, u):
    return _gp_upcross_density_stationary(xs, K, u, derivative_along_axes)


def _gp_upcross_density_stationary(xs, K, u, derivative_func):
    K_dxdy = derivative_func(K, [xs, xs], [1, 1])
    sigma_x = np.sqrt(np.diagonal(K))
    sigma_dx = np.sqrt(np.diagonal(K_dxdy))
    return 1/2/np.pi*sigma_dx/sigma_x*np.exp(-u**2/2/sigma_x**2)
