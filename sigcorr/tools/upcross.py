import numpy as np


def zero_upcross_count(sigs):
    return np.count_nonzero(zero_upcross_locs(sigs), axis=-1)


def zero_upcross_locs(sigs):
    return (sigs[..., 1:] > 0) & (sigs[..., :-1] <= 0)
