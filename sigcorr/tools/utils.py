import re
from functools import partial
from itertools import product
from concurrent.futures import ProcessPoolExecutor
import numpy as np
import scipy as sp
from scipy import optimize
import jax

from sigcorr.config import CFG


def get_ridge_width(model, corr, xrange):
    widths = []
    with ProcessPoolExecutor(10) as pool:
        for fitres in pool.map(partial(_fit_ridge, model, corr, xrange), range(model.xs[xrange].shape[0])):
            widths.append(fitres[0])
    widths = np.array(widths)
    return widths


def _fit_ridge(model, corr, xrange, bin_i):
    corr_window = model.xs.shape[0]//6

    def ridge_model(xs, std):
        return sp.stats.norm(loc=model.xs[xrange][bin_i], scale=std).pdf(xs)/sp.stats.norm(loc=0, scale=std).pdf(0)

    corr_window_slice = slice(max(0, bin_i-corr_window), bin_i+corr_window)
    return sp.optimize.curve_fit(ridge_model, model.xs[xrange][corr_window_slice], corr[bin_i][corr_window_slice])[0]


def produce_batch_ranges(total, batchsize):
    n_full_batches = int(total // batchsize)
    for i in range(n_full_batches):
        yield slice(i*batchsize, (i+1)*batchsize)
    last_batch_size = int(total % batchsize)
    if last_batch_size > 0:
        yield slice(total-last_batch_size, total)


def get_last_from_iter(it):
    i = None
    for i in it:
        pass
    return i


def parse_grid_file(path):
    """ Parse grid file into np array.

    Multidim arrays are stored flattened (``np.ravel``) in the file,
    use shape header to specify real array shape.
    """
    shape = None
    str_shape = None
    with open(path, "r") as fin:
        for line in fin:
            if not line:
                continue
            if not line.startswith("#"):
                break
            if not line.startswith("##shape"):
                continue
            str_shape = line[len("##shape"):].replace(")", "").replace("(", "")
    if str_shape is not None:
        sep = "x" if "x" in str_shape else "," if "," in str_shape else "\t" if "\t" in str_shape else " "
        shape = tuple([int(i.strip()) for i in str_shape.split(sep)])
    arr = np.genfromtxt(path, dtype=np.float64)
    if shape is not None:
        arr = arr.reshape(shape)
    return arr


def shape_to_line_index(shape):
    yield from range(np.product(shape))


def jax_jit_method(method):
    """Mark a method with this decorator for Jax to JIT compile it.

    Parameters
    ----------
    method :
        The method to be JIT compiled by Jax
    """
    return jax.jit(method, static_argnums=0)
