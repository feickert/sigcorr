import numpy as np
from sigcorr.models.gross_vitells import GrossVitells


class GrossVitellsAsimov(GrossVitells):
    def get_bg_samples(self, num_samples):
        expected_b = self.expected_b(self.B0)
        return expected_b.reshape(1, -1) \
             + np.sqrt(expected_b).reshape(1, -1).T * np.eye(expected_b.shape[0])
