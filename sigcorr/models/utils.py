from copy import deepcopy
import h5py


def set_params_from_file(model, input_file):
    with h5py.File(input_file, "r", swmr=True) as f:
        for p in model.PARAMS:
            val = f.attrs[p.lower()]
            setattr(model, p, val)
        model.xs = f["sampling_xs"][...]


def set_params_from_model(model, ref_model):
    for p in ref_model.PARAMS:
        val = deepcopy(getattr(ref_model, p))
        setattr(model, p, val)
    model.xs = ref_model.xs.copy()
