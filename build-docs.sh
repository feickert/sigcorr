#!/bin/bash

ROOT_DIR=$(dirname "$0")
PYTHONPATH=$ROOT_DIR sphinx-build -a -E -W -j4 -b html $ROOT_DIR/docs $ROOT_DIR/build/docs
