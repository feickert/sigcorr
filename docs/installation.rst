Installation
============

There are two ways to work with |project|.

* *Likelihood scans are produced and ready for use (tools mode).*
  There is no need to implement your model within |project|.
* *Model from scratch (development mode)*.
  You would need to add custom modules to the package.


Requirements
------------


The code was developed and run on Python 3.9.13, so any Python >= 3.9 should work, older versions of Python
may also work but will require "dev mode" installation with a change in the ``python_requires`` option in :src:`setup.cfg`.



For tools only
--------------


It is possible to install the package directly from Gitlab:

.. code-block:: bash

    pip install git+https://:@gitlab.cern.ch:8443/ananiev/sigcorr.git

You can also specify the commit/tag/branch to install from:

.. code-block:: bash

    pip install git+https://:@gitlab.cern.ch:8443/ananiev/sigcorr.git@master#egg=sigcorr-9999


.. NOTE::
    |project| uses `google/jax <https://github.com/google/jax>`_ for the just-in-time compilation of the loss functions.
    To increase the precision of the calculations special environment variables need to be set.
    We stored them in the :src:`env.sh` script, and suggest to ``source`` it each time
    before you start working with |project|:

    .. code-block:: bash

       source ./env.sh


For fitting the toys (dev mode)
-------------------------------


To be able to extend the |project| live, it is more convenient to install it
in the development mode ``pip install -e``. Then any changes to the code base will immediately be reflected
on the next run:

.. code-block:: bash

    git clone https://:@gitlab.cern.ch:8443/ananiev/sigcorr.git
    cd sigcorr
    pip install -e .


Test
----

Try running in the terminal the following command, you should see some help message:

.. code:: bash

    sigcorr-run -h

Alternatively, check that you can import something from the Python interpreter:

.. code::

    from sigcorr.tools.stats.utils import sig2pval
    print(sig2pval(3.))


Possible issues
----------------


If you are using Kerberos auth, make sure the token is available:

.. code-block:: bash

    kinit -F username@CERN.CH

You might need to enable ``http.emptyAuth`` for git:

.. code-block:: bash

    git config --global http.emptyAuth true
