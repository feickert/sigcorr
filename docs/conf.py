project = 'SigCorr'
author = 'Victor Ananyev (vindex10), Alex L. Read'
extensions = ['sphinx.ext.autodoc', 'sphinx.ext.mathjax', 'sphinx.ext.viewcode',
              'sphinx.ext.extlinks', 'sphinx.ext.intersphinx', 'numpydoc',
              'matplotlib.sphinxext.plot_directive', 'sphinx.ext.autosectionlabel']
autosectionlabel_prefix_document = True
highlight_language = 'python3'
autodoc_member_order = 'bysource'
numpydoc_show_class_members = False
html_theme = 'sphinx_rtd_theme'
autodoc_inherit_docstrings = False
special_members = "__init__"
extlinks = {"src": ("https://gitlab.cern.ch/ananiev/sigcorr/-/blob/master/%s", "/%s")}
intersphinx_mapping = {'python': ('https://docs.python.org/3', None),
                       'jax': ('https://jax.readthedocs.io/en/latest/', None),
                       'scipy': ('https://docs.scipy.org/doc/scipy/', None),
                       'numpy': ('https://numpy.org/doc/stable/', None)}

rst_prolog = f"""
.. |project| replace:: {project}
"""
