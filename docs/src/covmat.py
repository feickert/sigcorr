import h5py
import matplotlib.pyplot as plt
from sigcorr.tools.stats.utils import get_delta_sigs
from sigcorr.tools.stats.batch_stats import BatchStats2


bf_res = None
with h5py.File("../data/3K1D/hyy_tutorial.h5", "r") as fin:
    fields = ["b_loglikes", "sb_loglikes", "sb_params"]
    bf_res = {k: fin[k][:3000, ...] for k in fields}
    xs = fin["scan_xs"][...].ravel()


sigs = get_delta_sigs(bf_res["b_loglikes"], bf_res["sb_loglikes"], bf_res["sb_params"][..., 0])

num_samples = sigs.shape[0]
sample_dim = sigs.shape[1]
bs = BatchStats2(num_samples, sample_dim)
bs.push(sigs)
cov = bs.get_cov()
cov_err = bs.get_cov_stat_err()

fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(12, 4))

mesh = ax[0].pcolormesh(xs, xs, cov)
ax[0].set_xlabel("m, GeV")
ax[0].set_ylabel("m, GeV")
ax[0].invert_yaxis()
ax[0].set_title("Covariance matrix")
plt.colorbar(mesh, ax=ax[0])

mesh = ax[1].pcolormesh(xs, xs, cov_err)
ax[1].set_title("Statistical error")
ax[1].set_xlabel("m, GeV")
ax[1].set_ylabel("m, GeV")
ax[1].invert_yaxis()
plt.colorbar(mesh, ax=ax[1])


if __name__ == "__main__":
    plt.show()
