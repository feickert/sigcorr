import h5py
import numpy as np
import matplotlib.pyplot as plt
from sigcorr.tools.stats.utils import get_delta_sigs
from sigcorr.tools.stats.batch_stats import BatchStats2
from sigcorr.tools.stats.gp.sampling import draw_gp_svd
from sigcorr.tools.overflows import overflows
from sigcorr.tools.stats.utils import sig2pval


bf_res = None
with h5py.File("../data/3K1D/hyy_tutorial.h5", "r") as fin:
    fields = ["b_loglikes", "sb_loglikes", "sb_params"]
    bf_res = {k: fin[k][:3000, ...] for k in fields}
    xs = fin["scan_xs"][...].ravel()


sigs = get_delta_sigs(bf_res["b_loglikes"], bf_res["sb_loglikes"], bf_res["sb_params"][..., 0])

num_samples = sigs.shape[0]
sample_dim = sigs.shape[1]
bs = BatchStats2(num_samples, sample_dim)
bs.push(sigs)
cov = bs.get_cov()
cov_err = bs.get_cov_stat_err()

gp_sigs = draw_gp_svd(cov, 100_000)

local_sig_grid = np.arange(0, 3.05, 0.1)  # i.e. significances between 0 and 3 with step 0.1
local_p = sig2pval(local_sig_grid)

exceeds_or_not = overflows(gp_sigs, local_sig_grid)
global_p = np.mean(exceeds_or_not, axis=0)
tf = global_p/local_p
tf_err = np.std(exceeds_or_not, axis=0)/np.sqrt(gp_sigs.shape[0])/local_p

bf_exceeds_or_not = overflows(sigs, local_sig_grid)
bf_global_p = np.mean(bf_exceeds_or_not, axis=0)
bf_tf = bf_global_p/local_p
bf_tf_err = np.std(bf_exceeds_or_not, axis=0)/np.sqrt(sigs.shape[0])/local_p

plt.errorbar(local_sig_grid, tf, yerr=tf_err, label="TF from GP toys", color="orange", marker=".", capsize=2)
plt.errorbar(local_sig_grid, bf_tf, yerr=bf_tf_err, label="TF from MC toys", color="green",
             ls=None, marker=".", capsize=2)
plt.xlabel(r'$\mathrm{Z}_{local}$')
plt.ylabel("TF")
plt.title("Trials factor comparison")
plt.legend()


if __name__ == "__main__":
    plt.show()
