SigCorr
=======

|project| is a framework to study the trials factor. There are several known ways to estimate the trials factor:

From samples:

    * Produce enough Monte Carlo simulations to estimate the trials factor via brute force.
    * Produce enough Monte Carlo simulations to estimate the trials factor at lower significance,
      then use the Gross and Vitells procedure [1]_ to extrapolate it to higher local significance values.

From the covariance matrix of the significance curves:

    * Get the up-crossings density from the covariance matrix by using the analytical expression for the density. Then,
      up-crossings can be converted into the Gross and Vitells upper bound.
    * Use GP toys to estimate the average number of up-crossings at lower significances and then extrapolate with
      the Gross and Vitells scheme.
    * Use GP toys from the covariance matrix to estimate the trials factor via "efficient brute force".

One can assemble the approaches above from the ordered building blocks. Different approaches,
though, use different subsets of steps:

#. MC samples
#. Likelihood scans
#. Significance curves / Test statistic curves
#. Covariance matrix (brute force or using Asimov set of background samples [2]_)
#. GP samples
#. Up-crossings (direct or from GP samples)
#. Trials factor

|project| covers all these steps by providing useful utilities that consistently operate on defined data structures.
It is a framework, meaning, the user has to combine various utilities to build their own pipeline, that
might implement one of the approaches described above or even something new!


.. toctree::
   :maxdepth: 1
   :titlesonly:

   self
   installation
   design
   tutorial/tutorial
   api/api

References
----------

.. [1] V. Ananyev and A. L. Read, "Gaussian Process-based calculation of look-elsewhere trials factor,"
        https://arxiv.org/abs/2206.12328
.. [2] E. Gross and O. Vitells, "Trial factors for the look elsewhere effect in high energy physics",
        Eur. Phys. J. C 70, 525–530 (2010), https://doi.org/10.1140/epjc/s10052-010-1470-8
