Batch multiprocessing
=====================

We assume that at this point you have a model defined according to :doc:`/tutorial/model_definition`
and :doc:`/tutorial/grids`, and data were sampled and fitted as described in :doc:`/tutorial/fitting` chapter.
If you skipped the previous steps, just use :py:class:`~sigcorr.models.hyy.Hyy` for the model,
:src:`grids/hyy-common.dat` for the grid, and a 3000 fitted samples from this model on this grid
stored in :src:`docs/tutorial/data/hyy_tutorial.h5`.



*Motivation.* The file containing 1M fits for Hyy model has a size of 4GB. Gaussian process toys contain only
information about significance, so they weigh less (~500MB per 1M toys). To approach practical
precision at high significances (:math:`~7\sigma`) one might require ~100M toys and a finer grid.
For many kinds of questions it is possible to avoid loading entire dataset (~50GB) into RAM, by
reading it in batches and making partial calculations in batches and then aggregating the results.

Batches can be distributed into many workers (mapped) to be processed in parallel with the help of
the function-orchestrator :py:func:`~sigcorr.mapreduce.file.h5_batch_mapreduce`. An instance of
:py:class:`~sigcorr.mapreduce.map_reducers.Calculator` defines the operations applied to every
batch. Then the results of processing of each batch are aggregated (reduced) into the final value
using :py:class:`~sigcorr.mapreduce.map_reducers.Reducer`.

There is another orchestrator
:py:func:`~sigcorr.mapreduce.gp.gp_batch_mapreduce`. It was developed specifically to study
the Gaussian process approach to the modeling of the significance surfaces. Instead of reading the
significance surfaces from HDF5 file, :py:func:`~sigcorr.mapreduce.gp.gp_batch_mapreduce` generates
batches of the Gaussian process samples using the Gaussian process covariance matrix as an input
parameter. The same :py:class:`~sigcorr.mapreduce.map_reducers.Calculator`
/ :py:class:`~sigcorr.mapreduce.map_reducers.Reducer` interface can be used to compute summary
statistics on the Gaussian process samples.

Main steps of setting up the batch processing:

#. Choose the ``reader``:

    * samples from h5 file: :py:func:`~sigcorr.mapreduce.file.h5_batch_mapreduce`.

    * samples from GP: :py:func:`~sigcorr.mapreduce.gp.gp_batch_mapreduce`.

#. Build the pipeline of the processors: :py:class:`~sigcorr.mapreduce.map_reducers.Calculator`.

#. Choose the aggreagator: :py:class:`~sigcorr.mapreduce.map_reducers.Reducer`.

#. Exhaust the reader (:py:func:`~sigcorr.tools.utils.get_last_from_iter`). When the ``reader`` is called it returns
   a generator that yields a tuple with incremental intermediate results from the batches and the number of processed
   samples (!not batches).

The full list of available calculators and reducers can be found in
:py:mod:`sigcorr.mapreduce.map_reducers`. For example, the most common Calculator is
:py:class:`~sigcorr.mapreduce.map_reducers.SigsCalc`. It translates a batch of likelihoods into
a batch of statistical significances corresponding to these likelihoods.

Some basic math functions can be applied to the batch (e.g. ``np.reshape``) using
:py:class:`~sigcorr.mapreduce.map_reducers.MathCalc`. Calculators also can be chained with
:py:class:`~sigcorr.mapreduce.map_reducers.ChainCalc`, so that one can get a batch of indicators
whether significance field exceeds a particular threshold
(:py:class:`~sigcorr.mapreduce.map_reducers.OverflowsCalc`).


Estimating the covariance matrix. BatchStats multiprocessing
------------------------------------------------------------


We will use :py:func:`~sigcorr.mapreduce.file.h5_batch_mapreduce` to read samples from the file with likelihood fits.
We will provide names of the fields that will be extracted and fed to
the :py:class:`~sigcorr.mapreduce.map_reducers.SigsCalc` for significance curve calculation. Then, batches of the
significance curves will be aggregated with :py:func:`~sigcorr.mapreduce.map_reducers.BatchStats2Reduce`.

.. code::

    from sigcorr.mapreduce.file import h5_batch_mapreduce
    from sigcorr.mapreduce.map_reducers import SigsCalc
    from sigcorr.mapreduce.map_reducers import BatchStats2Reduce
    from sigcorr.tools.utils import get_last_from_iter

    bs, _ = get_last_from_iter(h5_batch_mapreduce("../data/hyy_tutorial.h5",
                                                  ["b_loglikes", "sb_loglikes", "sb_params"],
                                                  200,
                                                  SigsCalc(),
                                                  BatchStats2Reduce()))
    cov = bs.get_cov()
    cov_err = bs.get_cov_err()

Resulting plot from the batch multiprocessing that you can compare to the plot prepared manually in
the :ref:`previous section <tutorial/visualization_1d:Covariance matrix>`:

.. plot:: src/bs_covmat.py


Convergence of the average number of the GP sampled test statistic up-crossings
-------------------------------------------------------------------------------


We will sample 100K significance curves from the covariance matrix
with :py:func:`~sigcorr.mapreduce.gp.gp_batch_mapreduce`, then,
with the help of :py:class:`~sigcorr.mapreduce.map_reducers.MathCalc`, we will turn them into the test statistic curves.
The latter applies any function (in our case :py:func:`numpy.square`) to the input. Then we compute the average number
of up-crossings for the test statistic curves. For this we will chain two processors
:py:class:`~sigcorr.mapreduce.map_reducers.MathCalc` and :py:class:`~sigcorr.mapreduce.map_reducers.UpcrossCalc` using
:py:class:`~sigcorr.mapreduce.map_reducers.ChainCalc`.
During the calcualtion we will store the intermediate results from
:py:class:`~sigcorr.mapreduce.map_reducers.BatchStats1Reduce` to investigate the speed of convergence of the mean:

.. NOTE::
    Due to the specifics of implementation of the :py:mod:`~sigcorr.tools.stats.batch_stats`, intermediate values are
    still normalized to the total number of samples. We, therefore, rescale the numbers to get the correct value of
    the running mean.


.. code::

    from sigcorr.mapreduce.gp import gp_batch_mapreduce
    from sigcorr.mapreduce.map_reducers import ChainCalc
    from sigcorr.mapreduce.map_reducers import MathCalc
    from sigcorr.mapreduce.map_reducers import UpcrossCalc
    from sigcorr.mapreduce.map_reducers import BatchStats1Reduce

    ...
    nums = []
    avg_upcross = []
    pipeline = gp_batch_mapreduce(cov,
                                  100_000,  # num samples
                                  500,  # batch size
                                  xs.shape,  # sample shape
                                  ChainCalc([MathCalc(np.square), UpcrossCalc(1.)]),  # up-crossings at level 1.
                                  BatchStats1Reduce())
    for intermediate_bs, num_processed in pipeline:
        corrected_avg_upcross = intermediate_bs.get_mean()*intermediate_bs.n/num_processed
        avg_upcross.append(corrected_avg_upcross)
        nums.append(num_processed)

Resulting convergence plot with :math:`1\sigma` error band:

.. plot:: src/bs_running_upcross.py
