Tutorial
========

In this tutorial we will set the Gross and Vitells upper bound for the Trials factor for a toy model
consisting of a Gaussian signal peak search on top of the exponential background data.

The tutorial will guide you through the whole process
of using the |project| starting from the model definition and ending with the exploratory analysis
of the trials factors.

(requires the *development mode*) Defining the model, grids and doing the likelihood scans:

.. toctree::
   :maxdepth: 1
   :titlesonly:

   /tutorial/model_definition.rst
   /tutorial/grids.rst
   /tutorial/fitting.rst

(*tools mode* is enough) Using likelihood scans to compute significance curves, covariance matrices,
to sample GP toys and to estimate the trials factor:

.. toctree::
   :maxdepth: 1
   :titlesonly:

   /tutorial/visualization_1d.rst
   /tutorial/batch_multiprocessing.rst
   /tutorial/visualization_2d.rst
   /tutorial/asimov_set_of_bg_samples.rst
