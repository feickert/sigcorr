Model definition
================


:py:class:`~sigcorr.models.model.AbstractModel` in |project| consists of several required components:

* model parameters + ``init()`` method for them
* method for sampling background samples
* background model, its derivatives, starting point for the optimization
* signal + background model, its derivatives, starting point for the optimization

Full example of the defined model you can find on gitlab: :src:`sigcorr/models/hyy.py`. In this part of the tutorial
we will learn how to build this model from scratch.

We start by defining an empty class ``HyyTutorial`` for the model. If you installed |project| in the development mode,
then we would suggest to create the new model as part of the :src:`sigcorr/models` package. Name doesn't matter,
for example let's name it ``hyy_tutorial.py``.

.. code::

    from sigcorr.models.model import AbstractModel


    class HyyTutorial(AbstractModel):
        def __init__(self, xs):
            self.xs = xs


Background sampling
-------------------


Let's first define methods that are required for the simulation of toy background samples.
For the tutorial, the expected background model has an exponential shape with
two free parameters ``n_bg`` and ``xscale``:

.. code::

    import jax.numpy as jnp
    from sigcorr.tools.utils import jax_jit_method
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        @jax_jit_method
        def expected_b(self, n_bg, xscale):
            # shift by 100 GeV is applied to set the mass range around the real Higgs mass
            return n_bg*jnp.exp(-(self.xs-100)*xscale)

.. NOTE::
    We use `google/jax <https://github.com/google/jax>`_ for just-in-time compilation of the model functions.
    They will be evaluated a huge amount of times, so we give it a slight boost. To enable JAX we should use
    the wrapper around numpy (:py:mod:`jax.numpy`) and scipy (:py:mod:`jax.scipy`), that we do via alias ``jnp`` and ``jsp``,
    and also mark functions that need to be JIT-compiled with :py:func:`jax.jit`. In |project| we created a very simple decorator
    :py:func:`~sigcorr.tools.utils.jax_jit_method` with a reduced functionality, that allows to denote methods of a class
    as requiring JIT-compilation.

The method :py:meth:`~sigcorr.models.model.AbstractModel.get_bg_samples` is responsible for sampling toy background
realizations. For the background we assume that the bins are uncorrelated and the errors are Gaussian
with known standard deviation ``BG_NOISE_STD``. For toys we also set free parameters of the background
``n_bg = B0`` and ``xscale = self.BG_XSCALE``:

.. code::

    import scipy as sp
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        PARAMS = ["B0", "BG_XSCALE", "BG_NOISE_STD"]

        def __init__(self, xs):
            self.B0 = 10
            self.BG_XSCALE = 0.033
            self.BG_NOISE_STD = 0.3
            self.xs = xs

        def get_bg_samples(self, num_samples):
            return self.expected_b(self.B0, self.BG_XSCALE) + \
                   sp.stats.norm(scale=self.BG_NOISE_STD).rvs((num_samples, self.xs.shape[0]))

        @jax_jit_method
        def expected_b(self, n_bg, xscale):
            ...

.. NOTE::
    It is a good practice to store parameters as instance properties and to mention them in the ``PARAMS`` list.
    The parameters then will be stored as attributes in the output HDF5 file together with
    the background toy samples and fits.


Background fits
---------------

For the B-fits we need to define:

    * initial values for the parameters ``self.b_guess``
    * "minus log-likelihood" (farther we call it just "log-likelihood") function
      :py:meth:`~sigcorr.models.model.AbstractModel.b_minus_loglike`
    * its derivatives: the vector gradient (:py:meth:`~sigcorr.models.model.AbstractModel.b_minus_loglike_grad`)
      and the matrix Hessian (:py:meth:`~sigcorr.models.model.AbstractModel.b_minus_loglike_hess`).

We start with the log-likelihood. It is a function of a vector of optimized parameters ``p`` and
a ``sample`` of data:

.. code::

    import jax.numpy as jnp
    import jax.scipy as jsp
    from sigcorr.tools.utils import jax_jit_method
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        PARAMS = [..., "BG_NOISE_STD"]

        def __init__(self, xs):
            self.BG_NOISE_STD = 0.3
            self.xs = xs

        @jax_jit_method
        def b_minus_loglike(self, p, sample):
            return -jnp.sum(jsp.stats.norm.logpdf(sample,
                                                  loc=self.expected_b(*p),
                                                  scale=self.BG_NOISE_STD))

        @jax_jit_method
        def expected_b(self, n_bg, xscale):
            ...

Derivatives of the log-likelihood, similarly to the log-likelihood itself, are functions of
``p`` and ``sample``. These methods will be provided to :py:func:`scipy.optimize.minimize` for
the guided optimization and should follow the :py:mod:`scipy` guidelines.

.. code::

    import jax.numpy as jnp
    import jax.scipy as jsp
    from sigcorr.tools.utils import jax_jit_method
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        PARAMS = [..., "BG_NOISE_STD"]

        def __init__(self, xs):
            ...
            self.BG_NOISE_STD = 0.3
            self.xs = xs

        @jax_jit_method
        def b_minus_loglike_grad(self, p, sample):
            B_i = self.expected_b(1, p[1])
            mu_i = self.expected_b(*p)
            deviation = mu_i - sample
            dp0 = jnp.sum(deviation*B_i)
            dp1 = -jnp.sum(deviation*mu_i*(self.xs - 100))
            return jnp.hstack([dp0, dp1])/self.BG_NOISE_STD**2

        @jax_jit_method
        def b_minus_loglike_hess(self, p, sample):
            B_shape = self.expected_b(1, p[1])
            mu = p[0]*B_shape
            deviation = mu - sample
            B_exp = self.xs - 100
            dp0p0 = jnp.sum(B_shape**2)
            dp0p1 = jnp.sum(-B_exp*(deviation + mu)*B_shape)
            dp1p1 = jnp.sum((deviation + mu)*B_exp**2*mu)
            return jnp.array([[dp0p0, dp0p1],
                              [dp0p1, dp1p1]])/self.BG_NOISE_STD**2

        @jax_jit_method
        def expected_b(self, n_bg, xscale):
            ...

Finally, we need to define the initial guess ``self.b_guess`` that
is convenient to derive from the model parameters with a slight perturbation. For this reason,
we prefer setting the initial guesses for the parameters (``self.b_guess``) in
the :py:meth:`~sigcorr.models.model.AbstractModel.init` method. This makes it easier to automate the derivation
of the initial guesses from the predefined model parameters and secures user from forgetting to update initial guesses
when the model parameters were changed manually.

.. code::

    import numpy as np
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        PARAMS = ["B0", "BG_XSCALE", ...]

        def __init__(self, xs):
            self.B0 = 10
            self.BG_XSCALE = 0.033
            ...
            self.xs = xs

        def init(self):
            # we set the b_guess outside the local minimum (true values)
            # to avoid the optimizer stuck inside
            self.b_guess = np.array([self.B0, self.BG_XSCALE])*1.2
            return self


Signal + Background fits
------------------------


Similar components are required for SB-fits:

    * initial values for the parameters ``self.sb_guess``
    * "log-likelihood" function :py:meth:`~sigcorr.models.model.AbstractModel.sb_minus_loglike`
    * its derivatives: :py:meth:`~sigcorr.models.model.AbstractModel.sb_minus_loglike_grad`
      and :py:meth:`~sigcorr.models.model.AbstractModel.sb_minus_loglike_hess`.

The arguments of these functions, however, are a little bit more complex. For example,
:py:meth:`~sigcorr.models.model.AbstractModel.sb_minus_loglike`:


.. code::

    import jax.numpy as jnp
    import jax.scipy as jsp
    from sigcorr.tools.utils import jax_jit_method
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        PARAMS = [..., "BG_NOISE_STD", "SIG_STD"]

        def __init__(self, xs):
            ...
            self.SIG_STD = 5.
            self.BG_NOISE_STD = 0.3
            self.xs = xs

        @jax_jit_method
        def sb_minus_loglike(self, p, sample, signal_x):
            return -jnp.sum(jsp.stats.norm.logpdf(sample,
                                                  loc=self.expected_sb(signal_x, p[:1], p[1:]),
                                                  scale=self.BG_NOISE_STD))

        @jax_jit_method
        def expected_sb(self, sig_x, sig_params, bg_params):
            return self.expected_signal(sig_x, *sig_params) + self.expected_b(*bg_params)

        @jax_jit_method
        def expected_signal(self, signal_x, n_sig):
            return n_sig*jsp.stats.norm.pdf(self.xs, loc=signal_x, scale=self.SIG_STD)

        @jax_jit_method
        def expected_b(self, n_bg, xscale):
            ...

Main differences:

    * ``p`` is now a vector of both signal and background parameters.
      :py:meth:`~sigcorr.models.model.AbstractModel.sb_minus_loglike` and derivative methods are responsible
      for the separation.
    * ``signal_x`` that is one value in 1D case, but can be a list of parameters in more dimensions. These are the
      signal parameters that are not present under the background hypothesis.

.. NOTE::
   Methods ``expected_b``, ``expected_signal``, ``expected_sb`` are optional, however, are convenient
   for the definitions of not only log-likelihoods but also gradients and hessians.

Similarly we define the derivatives. SB-fit has 3 free parameters, therefore,
:py:meth:`~sigcorr.models.model.AbstractModel.sb_minus_loglike_grad` has output of length 3, and
:py:meth:`~sigcorr.models.model.AbstractModel.sb_minus_loglike_hess` returns a 3x3 matrix:

.. code::

    import jax.numpy as jnp
    import jax.scipy as jsp
    from sigcorr.tools.utils import jax_jit_method
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        PARAMS = [..., "BG_NOISE_STD"]

        def __init__(self, xs):
            ...
            self.BG_NOISE_STD = 0.3
            self.xs = xs

    @jax_jit_method
    def sb_minus_loglike_grad(self, p, sample, signal_x):
        S_i = self.expected_signal(signal_x, 1)
        B_i = self.expected_b(1, p[2])
        mu_i = self.expected_sb(signal_x, p[:1], p[1:])
        deviation = mu_i - sample
        dp0 = jnp.sum(deviation*S_i)
        dp1 = jnp.sum(deviation*B_i)
        dp2 = -jnp.sum(deviation*p[1]*B_i*(self.xs - 100))
        return jnp.hstack([dp0, dp1, dp2])/self.BG_NOISE_STD**2

    @jax_jit_method
    def sb_minus_loglike_hess(self, p, sample, signal_x):
        S_shape = self.expected_signal(signal_x, 1)
        B_shape = self.expected_b(1, p[2])
        B = p[1]*self.expected_b(1, p[2])
        mu = self.expected_sb(signal_x, p[:1], p[1:])
        deviation = mu - sample
        B_exp = (self.xs - 100)
        dp0p0 = jnp.sum(S_shape**2)
        dp0p1 = jnp.sum(S_shape*B_shape)
        dp0p2 = -jnp.sum(S_shape*B_exp*B)
        dp1p1 = jnp.sum(B_shape**2)
        dp1p2 = jnp.sum(-B_exp*(deviation + B)*B_shape)
        dp2p2 = jnp.sum((deviation + B)*B_exp**2*B)
        return jnp.array([[dp0p0, dp0p1, dp0p2],
                          [dp0p1, dp1p1, dp1p2],
                          [dp0p2, dp1p2, dp2p2]])/self.BG_NOISE_STD**2

        @jax_jit_method
        def expected_sb(self, sig_x, sig_params, bg_params):
            ...

        @jax_jit_method
        def expected_signal(self, signal_x, n_sig):
            ...

        @jax_jit_method
        def expected_b(self, n_bg, xscale):
            ...


Finally, we define the required ``self.sb_guess``:

.. code::

    import numpy as np
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        PARAMS = ["B0", "BG_XSCALE", ...]

        def __init__(self, xs):
            self.B0 = 10
            self.BG_XSCALE = 0.033
            ...
            self.xs = xs

        def init(self):
            # we set the sb_guess outside the local minimum (true values)
            # to avoid the optimizer stuck inside
            self.sb_guess = np.array([0.1, self.B0, self.BG_XSCALE])*1.2
            return self

With this definition we finalize the definition of the |project| model ``HyyTutorial``.
