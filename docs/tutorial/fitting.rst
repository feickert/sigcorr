Fitting the toys
================


We assume that you completed the :doc:`/tutorial/model_definition` and the :doc:`/tutorial/grids` chapters
of the tutorial, and you have the ``HyyTutorial`` model defined in the ``hyy_tutorial.py`` module
of the :src:`sigcorr/models` package of |project| installed locally in the development mode.

If you decided to skip the previous chapters, just make a copy of :src:`sigcorr/models/hyy.py` and rename the
file and the class inside. Also you can use this grid file :src:`grids/hyy-common.dat` for practicing.

We are going to use :src:`sigcorr-run` script to sample from the model and to fit these samples with the b- and sb-
models. To enable the newly created ``HyyTutorial`` model we need to add an extra import to the header of the script
and extend ``MODELS`` list with ``HyyTutorial`` class:


.. code::

    from sigcorr.models.hyy_tutorial import HyyTutorial

    MODELS = [..., HyyTutorial]


Basic run
---------


.. WARNING::
    Remember to ``source env.sh`` before running the fits.

Let's run:

.. code:: bash

    sigcorr-run -g grids/hyy-common.dat -o tmp/hyy_tutorial.h5 -n 1000 HyyTutorial

During the run you will see a progress bar twice, once for b-fits and once for sb-fits.
Examlple of such a progress bar:

.. code:: bash

    90%|██████████████████  | 900/1000 [00:05<00:02, 739.70it/s, writer_qsize=0, workers_qsize=1]


What we see:

* ``900/1000`` - number of fits processed out of the total number of fits. We ran ``sigcorr-run`` for ``-n 1000``,
  so we requested for ``1000`` samples, i.e. background fits (as in the example above). For sb-fits, the total number is
  a product of number of samples and scans per sample (scanning grid size).
* ``739 it/s`` - average performance of the fitter. How many fits (iterations) per second it produces.
* ``writer_qsize`` - number of processed batches waiting to be written to disk.
* ``workers_qsize`` - number of unprocessed batches waiting to be pulled and processed by workers.


There are more options available in :code:`sigcorr-run --help`. The most important ones are:

* ``-s`` - specify scanning grid if it is different from the sampling grid (provided with ``-g``)
* ``-f``, ``--force`` - overwrite output file. Default behavior is to raise an exception if output file exists.


Settings, batches and pools
---------------------------


The :py:class:`~sigcorr.fitter.Fitter`, that ``sigcorr-run`` uses under the hood, produces MC samples, splits them
into batches and feeds to workers that do likelihood fits. Then workers send results to the writer process that
writes output to the file.

By default batch sizes and worker pool sizes are kept small, to be able to run on a laptop. However, on a powerful node
you might want to scale up. You can override default values for batch sizes and pool sizes using environment variables:

.. code:: bash

    export SIGCORR_FITTER_bfit_batchsize=300
    export SIGCORR_FITTER_bfit_pool_size=70

    export SIGCORR_FITTER_sbfit_batchsize=5
    export SIGCORR_FITTER_sbfit_pool_size=250

    sigcorr-run -g grids/hyy-common.dat -o tmp/hyy_tutorial.h5 -n 3000000 HyyTutorial


With this setup there will be 70 workers to process background fits, each worker will receive batches of 300 samples
and will do 300 background fits. With 3M samples, each worker will receive on average 140 batches.

For the signal+background fits, however, the pool consists of 250 workers. One batch contains 5 samples, but each sample
requires 61 signal+background fit, therefore, one batch results in ~300 fits. On average, each worker will process
2400 batches.


It is reasonable to set larger batch size for the background fits because only 1 fit per sample is produced there,
while the sb-fits might require a lower number of samples in a batch to compensate for the scan. Rule of thumb would be
``bfit_batch_size * signal_grid_size = sbfit_batch_size``.


These were the most oftenly used options, see more in :src:`sigcorr/config.py`.
