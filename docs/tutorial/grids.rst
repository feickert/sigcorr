Grids
=====


For the toy Hyy study with ``HyyTutorial`` model implemented in the :doc:`/tutorial/model_definition` chapter,
we would like to sample MC data samples on the grid of ``100-160 GeV`` with a step of ``1 GeV``. And we want to use
the same points for the signal peak location while conducting the sb-likelihood scan.

There are also two kinds of grids that we need to define: ``sampling`` and ``scanning``. The ``sampling`` grid
defines the bin centers at which we sample toy bin counts
(see :py:meth:`~sigcorr.models.model.AbstractModel.get_bg_samples`), while the ``scanning`` grid defines values of the
signal nuisance parameters that are not present under the background hypothesis. We do the likelihood scan along the
``scanning`` grid. The shapes of these grids can be quite flexible.

For example, the shape of the ``sampling`` grid is used in the definition of the model to compute the likelihoods.
There are no other places in |project| that strictly depend on the ``sampling`` grid. Consequently, your sampling grid
should be consistent with your model definition, no more than that.

The ``scanning`` grid's last dimension should contain a vector of the signal nuisance parameters, they are passed as
``signal_x`` to the :py:meth:`~sigcorr.models.model.AbstractModel.sb_minus_loglike` and other sb- methods of the model.
The rest of the dimensions enumerate different scan points. Result of the sb-likelihood fit will have a shape
of the ``scanning`` without the last dimension (i.e. signal parameters were replaced with the likelihood value).

The :src:`sigcorr-run` script can read the grid from file with :py:func:`~sigcorr.tools.utils.parse_grid_file`.
The file should start with a header that defines the shape of the array, and then a space separated list of numbers.
Here is an example of the grid that we will use for our Hyy toy study
(``...`` is a placeholder for the docs, numbers should be explicit in the grid):

.. code::

    ##shape -1,1
    100.0 101 102 103 104 105 106 ... 158 159 160.0

The ``.reshape(-1, 1)`` will be applied and the resulting grid will look as follows:

.. code::

    array([[100.],
           [101.],
           [102.],
           [103.],
           ...
           [157.],
           [158.],
           [159.],
           [160.]])

This is exactly the grid that we would want to use as a ``scanning`` grid, however, to use it as a sampling grid
the array should be flattened. Since the only place where we use the ``sampling`` grid is the model,
let's flatten it there and use the same grid file for both the ``scanning`` and the ``sampling`` grids.
It will require an extra line in the ``hyy_tutorial.py`` in the :py:meth:`~sigcorr.models.model.AbstractModel.init`
method of the model definition:

.. code::

    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        def __init__(self, xs):
            self.xs = xs
            ...

        def init(self):
            self.xs = self.xs.ravel()
            ...
            return self

The final file with the grid is :src:`grids/hyy-common.dat`, while the model ``HyyTutorial`` should precisely match
the :src:`sigcorr/models/hyy.py`.
