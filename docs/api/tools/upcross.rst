Upcross
=======

.. automodule:: sigcorr.tools.upcross
   :undoc-members:
   :members: zero_upcross_count, zero_upcross_locs
