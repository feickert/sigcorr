Gaussian process
================

.. automodule:: sigcorr.tools.stats.gp

.. toctree::
   /api/tools/stats/gp/sampling
   /api/tools/stats/gp/upcross
   /api/tools/stats/gp/euler_number
