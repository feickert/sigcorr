Batch managers
==============

.. autofunction:: sigcorr.mapreduce.file.h5_batch_mapreduce

.. autofunction:: sigcorr.mapreduce.gp.gp_batch_mapreduce
