MapReduce
=========

.. toctree::
   /api/mapreduce/batch_managers
   /api/mapreduce/map_reducers
