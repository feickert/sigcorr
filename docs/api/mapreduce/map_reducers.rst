MapReduce building blocks (MapReducers)
=======================================

.. automodule:: sigcorr.mapreduce.map_reducers
   :members:
   :special-members: __init__
