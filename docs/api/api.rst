API
===

.. toctree::
   /api/model
   /api/fitter
   /api/mapreduce/mapreduce
   /api/tools/tools
